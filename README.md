Depends on a core patch:

"Add a way to silently keep an item locked when processing a queue via cron"
https://www.drupal.org/files/issues/2020-08-19/3116478-63.patch

This patch allows requeueing items if for example not all the translations 
belonging to a group are ready.
