<?php

namespace Drupal\etranslation\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class etranslationTranslationSettingsForm.
 *
 * The settings form for the etranslation configuration.
 */
class EtranslationSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Construct a form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_type_bundle_info
   *   The entity bundle info service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfo $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'etranslation.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'etranslation_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('etranslation.settings');

    $entity_types = $this->entityTypeManager->getDefinitions();
    foreach ($entity_types as $entity_type => $entity_type_definition) {
      if ($entity_type_definition->entityClassImplements(FieldableEntityInterface::class)) {
        $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type);
        foreach ($bundles as $bundle => $bundle_definition) {
          $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
          $text_fields = array_filter($fields, function ($field) {
            return $field->getType() == 'text_long' || $field->getType() == 'string';
          });

          if (!empty($text_fields)) {
            $available[$entity_type][$bundle] = $text_fields;
          }
        }
      }
    }

    $form['disable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable'),
      '#description' => $this->t('Check this box to disable the translation system.'),
      '#default_value' => $config->get('disable'),
    ];

    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MT@EC API Service Endpoint'),
      '#default_value' => $config->get('api_url') ?? "https://webgate.ec.europa.eu/etranslation/si/translate",
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MT@EC API Username'),
      '#maxlength' => 64,
      '#size' => 64,
      '#placeholder' => $config->get('username'),
      '#default_value' => $config->get('username'),
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('MT@EC API Password'),
      '#maxlength' => 64,
      '#size' => 64,
      '#placeholder' => $config->get('password') ? '*** API Key ***' : '',
      '#default_value' => $config->get('password'),
    ];

    $form['entity_types'] = [
      '#type' => 'details',
      '#title' => 'Fields where to show translation link',
      '#tree' => TRUE,
    ];

    $default_value = $config->get('entity_types');

    foreach ($available as $entity_type => $bundles) {
      $label = $this->entityTypeManager->getDefinition($entity_type)->getLabel()->__toString();
      $form['entity_types'][$entity_type] = [
        '#type' => 'fieldset',
        '#title' => $label,
      ];
      foreach ($bundles as $bundle => $fields) {
        $label = $this->getBundleLabel($entity_type, $bundle);
        $form['entity_types'][$entity_type][$bundle] = [
          '#type' => 'fieldset',
          '#title' => $label,
        ];
        foreach ($fields as $field => $field_definition) {
          $form['entity_types'][$entity_type][$bundle][$field] = [
            '#type' => 'checkbox',
            '#title' => "{$field}",
            '#default_value' => $default_value[$entity_type][$bundle][$field] ?? FALSE,
          ];
        }
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $settings = $this->config('etranslation.settings');

    $password = $form_state->getValue('password');
    if (empty($password)) {
      $password = $settings->get('password');
    }

    $entity_types = $form_state->getValue('entity_types');
    $entity_types = _etranslation_array_filter_recursive($entity_types);

    $settings
      ->set('username', $form_state->getValue('username'))
      ->set('password', $password)
      ->set('entity_types', $entity_types)
      ->set('disable', $form_state->getValue('disable'))
      ->save();
  }

  /**
   * Helper.
   *
   * Get bundle label.
   */
  private function getBundleLabel($entity, $type) {
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity);
    if (isset($bundles[$type])) {
      return $bundles[$type]['label'];
    }
  }

}
