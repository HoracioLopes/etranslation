<?php

namespace Drupal\etranslation\Plugin\Action;

use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\TypedData\TranslatableInterface;

/**
 * Convert enquiry to booking.
 *
 * @Action(
 *   id = "node_get_translations",
 *   label = @Translation("Get translations"),
 *   type = "node"
 * )
 */
class GetTranslations extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * GetTranslation action constructor.
   *
   * @param array $configuration
   *   The action configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Current user account.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    ConfigFactoryInterface $config_factory,
    AccountInterface $account,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->account = $account;
    $this->loggerFactory = $logger_factory->get('etranslation');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = $object->access('update', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $langcodes = \Drupal::languageManager()->getLanguages();
    $langcodesList = array_keys($langcodes);
    return ['languages' => $langcodesList];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $langcodes = \Drupal::languageManager()->getLanguages();
    $options = [];
    foreach ($langcodes as $langcode) {
      $options[$langcode->getId()] = $langcode->getName();
    }

    $form['languages'] = [
      '#title' => $this->t('Languages'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $options,
      '#multiple' => TRUE,
      '#default_value' => $this->configuration['languages'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['title'] = $form_state->getValue('title');
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if (!$entity) {
      return;
    }

    $langcodesList = $this->getConfiguration()['languages'];

    if (($key = array_search($entity->language()->getId(), $langcodesList)) !== FALSE) {
      unset($langcodesList[$key]);
    }

    foreach ($langcodesList as $langcode) {
      $uuid = \Drupal::service('uuid')->generate();
      $this->fetchEntityTranslation($uuid, $entity, $langcode);
      $queue = \Drupal::queue('etranslation');
      $queue->createQueue();
      $queue->createItem($uuid);
    }
  }

  /**
   * Request a translation for an entity and all referenced entities within.
   *
   * @param string $uuid
   *   An uuid. Used to group jobs.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to translate.
   * @param string $destLang
   *   The target language of the translation. Iso 2 language code.
   *
   * @throws Exception
   */
  private function fetchEntityTranslation($uuid, EntityInterface $entity, $destLang = NULL) {

    if (!$entity || !$destLang) {
      return;
    }

    $target_language = strtok($destLang, '-');

    foreach ($entity->referencedEntities() as $ref) {
      $isTranslatable = \Drupal::service('content_translation.manager')->isEnabled($ref->getEntityTypeId(), $ref->bundle());
      if ($ref instanceof TranslatableInterface && $isTranslatable) {
        $translatable = $this->getTranslatableFields($ref);
        foreach ($translatable as $field_name => $field_config) {
          \Drupal::service('etranslation.request_translation')
            ->requestTranslation($uuid, $ref, $field_name, $target_language);
        }
      }
    }

    $translatable = $this->getTranslatableFields($entity);

    foreach ($translatable as $field_name => $field_config) {
      \Drupal::service('etranslation.request_translation')
        ->requestTranslation($uuid, $entity, $field_name, $target_language);
    }
  }

  /**
   * Get an entity translatable fields.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   A drupal entity.
   */
  private function getTranslatableFields(EntityInterface $entity) {
    $translatable = $entity->getTranslatableFields();
    unset($translatable['langcode']);
    unset($translatable['default_langcode']);
    unset($translatable['revision_translation_affected']);
    unset($translatable['content_translation_source']);
    unset($translatable['content_translation_outdated']);
    unset($translatable['content_translation_changed']);
    unset($translatable['uid']);
    return $translatable;
  }

}
