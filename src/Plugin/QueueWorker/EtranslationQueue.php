<?php

namespace Drupal\etranslation\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\DelayedRequeueException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Translations.
 *
 * @QueueWorker(
 *   id = "etranslation",
 *   title = @Translation("ETranslation queue"),
 *   cron = {"time" = 60}
 * )
 */
class EtranslationQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $count = $this->connection->select('etranslation', 'et')
      ->fields('et')
      ->condition('uuid', $data)
      ->condition('error', 0, '<>')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($count) {
      throw new DelayedRequeueException(10);
    }

    $rows = $this->connection->select('etranslation', 'et')
      ->fields('et')
      ->condition('uuid', $data)
      ->execute()
      ->fetchAll();

    foreach ($rows as $row) {
      $this->processJobId($row->job_id);
    }
  }

  /**
   * Process a job id.
   *
   * @param string $jobId
   *   The job id to process.
   */
  private function processJobId($jobId) {

    $q = $this->connection->select('etranslation', 'met');
    $q->fields('met');
    $q->condition('met.job_id', $jobId);

    $translation_data = $q
      ->execute()
      ->fetchAll();

    if (!empty($translation_data)) {
      $item = array_pop($translation_data);

      $entity = $this->entityTypeManager
        ->getStorage($item->entity_type)
        ->load($item->entity_id);

      // Map language to drupal.
      if ($item->language == 'pt') {
        $item->language = 'pt-pt';
      }

      $translation = $entity->hasTranslation($item->language)
        ? $entity->getTranslation($item->language)
        : $entity->addTranslation($item->language, $entity->toArray());

      $translation->{$item->field_name}->value = $item->translation;
      $translation->save();

      // Delete job.
      $this->connection->delete('etranslation')
        ->condition('job_id', $jobId)
        ->execute();
    }
  }

}
