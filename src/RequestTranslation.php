<?php

namespace Drupal\etranslation;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * A service.
 */
class RequestTranslation extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $request = $container->get('request_stack');
    $connection = $container->get('database');
    $entity_type_manager = $container->get('entity_type.manager');
    $config = $container->get('config.factory')->get('etranslation.settings');
    $current_user = $container->get('current_user');
    $http_client = $container->get('http_client');
    return new static($request, $connection, $entity_type_manager, $config, $current_user, $http_client);
  }

  /**
   * EtranslationTranslation constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \GuzzleHttp\Client $http_client
   *   The http client.
   */
  public function __construct(
    RequestStack $request,
    Connection $connection,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    Client $http_client
  ) {
    $this->request = $request;
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('etranslation.settings');
    $this->currentUser = $current_user;
    $this->httpClient = $http_client;
  }

  /**
   * Request a translation from the etranslation service.
   *
   * @param string $uuid
   *   A uuid to group the tranlastion jobs.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to translate.
   * @param string $field_name
   *   The field to translate.
   * @param string $target_language
   *   The language to translate to. Lowercase ISO 2 language code.
   */
  public function requestTranslation($uuid, EntityInterface $entity, $field_name, $target_language) {

    $source_language = $entity->language()->getId();

    foreach ($entity->{$field_name} as $delta => $text) {
      // Handle existing translations.
      $url = $this->config->get('api_url');
      $username = $this->config->get('username');
      $password = $this->config->get('password');

      $user = $this->currentUser->isAuthenticated()
        ? $this->currentUser->getAccount()->getAccountName()
        : 'Anonymous User';

      $host = $this->request->getCurrentRequest()->getSchemeAndHttpHost();

      if (strpos($host, 'localhost') !== FALSE) {
        drupal_set_message($this->t("The host where this request originates from needs to be reachable by the translation server."), 'error');
        return;
      }

      $data = [
        'documentToTranslateBase64' => [
          'content' => base64_encode($text->value),
          'format' => 'html',
        ],
        'sourceLanguage' => $source_language,
        'domain' => 'GEN',
        'callerInformation' => [
          'application' => $username,
          'username' => $user,
        ],
        'targetLanguages' => [
          $target_language,
        ],
        'destinations' => [
          'httpDestinations' => [
            $host . "/translation/callback",
          ],
        ],
        'error-callback' => $host . "/translation/error",
      ];

      $response = $this->httpClient->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
        ],
        'auth' => [$username, $password, 'digest'],
        'json' => $data,
      ]);

      if ($response->getStatusCode() == 200) {
        $jobId = $response->getBody()->getContents();
        $fields = ['translation' => '', 'date' => time()];
        $fields += [
          'uuid' => $uuid,
          'job_id' => ($jobId > 0) ? $jobId : 0,
          'error' => ($jobId < 0) ? $jobId : $response->getStatusCode(),
        ];

        $this->connection->merge('etranslation')
          ->key([
            'entity_type' => $entity->getEntityTypeId(),
            'entity_id' => $entity->id(),
            'field_name' => $field_name,
            'delta' => $delta,
            'language' => $target_language,
          ])
          ->fields($fields)
          ->execute();

        return $jobId;
      }
    }
    return TRUE;
  }

}
