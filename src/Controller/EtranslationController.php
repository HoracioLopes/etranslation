<?php

namespace Drupal\etranslation\Controller;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * An controller.
 */
class EtranslationController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $connection = $container->get('database');
    $entity_type_manager = $container->get('entity_type.manager');
    $config = $container->get('config.factory')->get('etranslation.settings');
    $current_user = $container->get('current_user');
    $logger = $container->get('logger.factory')->get('etranslation');
    $http_client = $container->get('http_client');
    $event_dispatcher = $container->get('event_dispatcher');
    return new static($connection, $entity_type_manager, $config, $current_user, $logger, $http_client, $event_dispatcher);
  }

  /**
   * EtranslationTranslation constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The module config.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger.
   * @param \GuzzleHttp\Client $http_client
   *   The http client.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    Connection $connection,
    EntityTypeManagerInterface $entity_type_manager,
    ImmutableConfig $config,
    AccountInterface $current_user,
    LoggerInterface $logger,
    Client $http_client,
    ContainerAwareEventDispatcher $event_dispatcher
  ) {
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config;
    $this->currentUser = $current_user;
    $this->logger = $logger;
    $this->httpClient = $http_client;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Endpoint to receive translations from the webservice.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return array
   *   Irrelevant.
   *
   * @throws \Exception
   */
  public function receiveTranslation(Request $request) {
    $jobId = $request->get('request-id');
    $encoded_translation = $request->getContent();
    $translation = base64_decode($encoded_translation);

    $query = $this->connection->select('etranslation', 'm')
      ->fields('m')
      ->condition('job_id', $jobId);

    $row = $query->execute()->fetchAll();

    if (count($row)) {
      $this->connection->merge('etranslation')
        ->key([
          'job_id' => $jobId,
        ])
        ->fields([
          'translation' => $translation,
          'date' => time(),
          'error' => 0,
        ])
        ->execute();
    }
    else {
      $this->logger->error("Job ID " . $jobId . " not found.");
    }

    // Doesn't matter, it's just a POST anyway.
    return [];

  }

  /**
   * Endpoint to receive translations from the webservice.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return array
   *   Irrelevant.
   *
   * @throws \Exception
   */
  public function receiveTranslationError(Request $request) {
    $jobId = $request->get('request-id');
    $targetLanguage = $request->get('target-language');
    $this->logger->error("Job ID " . $jobId . " not found. Error code " . $request->get('error-code'));

    $query = $this->connection->select('etranslation', 'm')
      ->fields('m');
    $query->condition('job_id', $jobId);
    $count = $query->countQuery()->execute()->fetchField();

    if ($count) {
      $this->connection->merge('etranslation')
        ->key([
          'job_id' => $jobId,
          'language' => $targetLanguage,
        ])
        ->fields([
          'date' => time(),
          'error' => $request->get('error-code'),
        ])
        ->execute();
    }
    else {
      $this->logger->error("Job ID " . $jobId . " not found. Error code " . $request->get('error-code'));
    }

    return [];
  }

}
