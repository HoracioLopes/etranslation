<?php

namespace Drupal\etranslation;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\locale\SourceString;
use Drupal\locale\StringStorageInterface;

/**
 * Defines a helper class for importing default strings.
 *
 * @internal
 *   This code is only for use by the MT@EC module.
 */
class InstallHelper implements ContainerInjectionInterface {

  /**
   * The locale storage.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  protected $localeStorage;

  /**
   * Constructs a new InstallHelper object.
   *
   * @param \Drupal\locale\StringStorageInterface $locale_storage
   *   The locale storage to use for reading string translations.
   */
  public function __construct(StringStorageInterface $locale_storage) {
    $this->localeStorage = $locale_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('locale.storage')
    );
  }

  /**
   * Helper to manually add a single translation string.
   *
   * @param string $source_string
   *   Source string.
   * @param string $langcode
   *   The langcode.
   * @param string $translated_string
   *   Translated string.
   */
  private function addTranslation($source_string, $langcode, $translated_string) {
    // Find existing source string.
    $storage = $this->localeStorage;
    $string = $storage->findString(['source' => $source_string]);
    if (is_null($string)) {
      $string = new SourceString();
      $string->setString($source_string);
      $string->setStorage($storage);
      $string->save();
    }
    // Create translation. If one already exists, it will be replaced.
    $translation = $storage->createTranslation([
      'lid' => $string->lid,
      'language' => $langcode,
      'translation' => $translated_string,
    ]);
    $translation->save();
  }

  /**
   * Import translations.
   */
  public function importTranslations() {

    $strings = [
      "Don't show again" => [
        'bg' => 'Не показвай повече',
        'de' => 'Nicht mehr anzeigen',
        'fr' => 'Ne plus afficher',
        'it' => 'Non mostrare più',
        'nl' => 'Niet meer laten zien',
        'pt-pt' => 'Não mostrar novamente',
        'cs' => 'Nezobrazovat znovu',
        'da' => 'Ikke vis igjen',
        'et' => 'Ära enam näita',
        'el' => 'Να μην εμφανιστεί ξαν',
        'es' => 'No mostrar de nuevo',
        'ga' => 'Ná taispeáin arís',
        'hr' => 'Ne pokazuj više',
        'sv' => 'Visa inte igen',
        'fi' => 'Älä näytä uudelleen',
        'sl' => 'Ne prikazuj več',
        'sk' => 'Už sa nezobrazovať',
        'ro' => 'Nu mai arăta din nou',
        'ml' => 'Terġax turi',
        'hu' => 'Ne mutasd újra',
        'lt' => 'Nerādīt vairs',
        'lv' => 'Daugiau nerodyti',
      ],
    ];

    foreach ($strings as $string => $translations) {
      foreach ($translations as $code => $translation) {
        $this->addTranslation($string, $code, $translation);
      }
    }

  }

}
