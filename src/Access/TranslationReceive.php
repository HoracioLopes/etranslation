<?php

namespace Drupal\etranslation\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Connection;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class TranslationReceive.
 *
 * Access checks to receive translation endpoint.
 *
 * @package Drupal\etranslation\Access
 */
class TranslationReceive implements AccessInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * TranslationRequest constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The current request stack.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(RequestStack $request, AccountInterface $account, Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, RequestStack $request, AccountInterface $account) {
    $jobId = $request->getCurrentRequest()->query->get('request-id');

    // Check if the request contains translation_id.
    if (isset($jobId) && !empty($jobId)) {
      $count = $this->connection->select('etranslation', 'et')
        ->fields('et')
        ->condition('job_id', $jobId)
        ->condition('error', 200)
        ->countQuery()
        ->execute()
        ->fetchField();

      return $count
        ? AccessResult::allowed()
        : AccessResult::forbidden('Non-existent Job ID.');
    }
    else {
      return AccessResult::forbidden('Translation receive missing information: Job ID.');
    }
    return AccessResult::forbidden('Translation blocked.');
  }

}
